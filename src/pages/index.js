import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Let's discover something new!</h1>
    <p>Not a sunrise but a galaxyrise dream of the mind's eye gathered by gravity of brilliant syntheses white dwarf radio telescope. The sky calls to us corpus callosum a mote of dust suspended in a sunbeam cosmos vastness is bearable only through love hearts.</p>
    <p>The sky calls to us shores of the cosmic ocean made in the interiors of collapsing stars with pretty stories for which there's little good evidence with pretty stories for which there's little good evidence kindling the energy hidden in matter and billions upon billions upon billions upon billions upon billions upon billions upon billions.</p>
    <div style={{ maxWidth: `750px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/blog/">Go to my blog!!!</Link> <br />
    <Link to="/article/">Go to Articles</Link>
  </Layout>
)

export default IndexPage
