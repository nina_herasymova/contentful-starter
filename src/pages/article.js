import React from "react"
import {useStaticQuery, graphql, Link} from "gatsby"

import Img from "gatsby-image"
import Layout from "../components/layout"
import SEO from "../components/seo"

const Article = () => {
  const data = useStaticQuery(
    graphql`
      query {
       allContentfulArticle {
    edges {
      node {
        title
        id
        slug
        publishedDate(formatString: "Do MMMM, YYYY")
        excerpt
        author
        banner{
                fluid(maxWidth: 300) {
                  ...GatsbyContentfulFluid
                }
              }
      }
    }
  }
      }
    `
  )
  return (
    <Layout>
      <SEO title="Article"/>
      <p>
        <Link to="/">Go back to the homepage</Link>
      </p>
      <ul className="posts">
        {data.allContentfulArticle.edges.map(edge => {
          return (
            <li className="post" key={edge.node.id}>
              <h2>
                <Link to={`/article/${edge.node.slug}/`}>{edge.node.title}</Link>
              </h2>
              <div className="meta">
                <span>Posted by {edge.node.author} on {edge.node.publishedDate}</span>
              </div>
              <div className="post_preview">
                {edge.node.banner && (
                  <Img
                    className="featured_preview"
                    fluid={edge.node.banner.fluid}
                    alt={edge.node.title}
                  />
                )}
              </div>
              <p className="excerpt">
                {edge.node.excerpt}
              </p>
                <div className="button">
                <Link to={`/article/${edge.node.slug}/`}>Read More</Link>
              </div>
            </li>
          )
        })}
      </ul>
    </Layout>
  )
}

export default Article
