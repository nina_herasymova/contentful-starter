import React from "react"
import { graphql, Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"
import Img from "gatsby-image";

export const query = graphql`
  query($slug: String!) {
    contentfulArticle(slug: { eq: $slug }) {
      title
      publishedDate(formatString: "Do MMMM, YYYY")
       banner {
          fluid(maxWidth: 750) {
          ...GatsbyContentfulFluid
        }
        }
      content {
        json
      }
    }
  }
`

const SingleArticle = props => {

  const options = {
    renderNode: {
      "embedded-asset-block": node => {
        const alt = node.data.target.fields.title["en-US"]
        const url = node.data.target.fields.file["en-US"].url
        return <img alt={alt} src={url} />
      },
    },
  }

  return (
    <Layout>
      <SEO title={props.data.contentfulArticle.title} />
      <Link to="/article/">Return to the articles</Link>
      <div className="content">
        <h1>{props.data.contentfulArticle.title}</h1>
        <span className="meta">
          Posted on {props.data.contentfulArticle.publishedDate}
        </span>
        {props.data.contentfulArticle.banner && (
          <Img
            className="featured"
            fluid={props.data.contentfulArticle.banner.fluid}
            alt={props.data.contentfulArticle.title}
          />
        )}
        {documentToReactComponents(props.data.contentfulArticle.content.json, options)}
      </div>
    </Layout>
  )
}

export default SingleArticle
